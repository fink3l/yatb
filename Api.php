<?php

declare(strict_types=1);

namespace Yatb;

use TelegramBot\Api\BotApi;

class Api implements ApiInterface
{
    /**
     * @var BotApi
     */
    private $client;

    /**
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->client = new BotApi($token);
    }

    /**
     * @inheritdoc
     */
    public function getUpdates(int $offset, int $limit, int $timeout): array
    {
        return $this->client->getUpdates($offset, $limit, $timeout);
    }

    /**
     * @inheritdoc
     */
    public function setWebhook(string $url, string $certificate = null): string
    {
        return $this->client->setWebhook($url, $certificate);
    }
}
