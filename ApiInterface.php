<?php

namespace Yatb;

use TelegramBot\Api\Types\Update;

interface ApiInterface
{
    /**
     * @param int $offset
     * @param int $limit
     * @param int $timeout
     *
     * @throws \TelegramBot\Api\Exception
     * @throws \TelegramBot\Api\InvalidArgumentException
     *
     * @return array|Update[]
     */
    public function getUpdates(int $offset, int $limit, int $timeout): array;

    /**
     * @param string $url
     * @param string|null $certificate
     *
     * @throws \TelegramBot\Api\Exception
     *
     * @return string
     */
    public function setWebhook(string $url, string $certificate = null): string ;
}
