<?php

namespace Yatb\Model;

interface ApiInterface
{
    /**
     * @param array $data
     *
     * @return ApiInterface
     */
    public function setData(array $data): ApiInterface;
}
