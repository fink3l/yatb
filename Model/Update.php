<?php

declare(strict_types=1);

namespace Yatb\Model;

class Update implements UpdateInterface
{
    /**
     * @var array
     */
    private $data;

    /**
     * @inheritdoc
     */
    public function setData(array $data): ApiInterface
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getText(): ?string
    {
        return $this->data['text'];
    }

    /**
     * @inheritdoc
     */
    public function getType(): string
    {
        return $this->data['type'];
    }

    /**
     * @inheritdoc
     */
    public function getCommand(): string
    {
        return $this->data['command'];
    }

    /**
     * @inheritdoc
     */
    public function getUserId(): int
    {
        return $this->data['userId'];
    }

    /**
     * @inheritdoc
     */
    public function getUsername(): string
    {
        return $this->data['username'];
    }

    /**
     * @inheritdoc
     */
    public function getChatId(): int
    {
        return $this->data['chatId'];
    }
}
