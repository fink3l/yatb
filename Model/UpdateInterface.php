<?php

namespace Yatb\Model;

interface UpdateInterface extends ApiInterface
{
    /**
     * @return null|string
     */
    public function getText(): ?string;

    /**
     * @return string
     */
    public function getCommand(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return int
     */
    public function getUserId(): int;

    /**
     * @return string
     */
    public function getUsername(): string;

    /**
     * @return int
     */
    public function getChatId(): int;
}
