<?php

declare(strict_types=1);

namespace Yatb;

use Yatb\Model\UpdateInterface;
use Yatb\Service\UpdateGeneratorInterface;
use Yatb\Service\WebhookManagerInterface;

class Client implements ClientInterface
{
    /**
     * @var UpdateGeneratorInterface
     */
    private $updateGenerator;

    /**
     * @var WebhookManagerInterface
     */
    private $webhookManager;

    /**
     * @param UpdateGeneratorInterface $updateGenerator
     * @param WebhookManagerInterface $webhookManager
     */
    public function __construct(UpdateGeneratorInterface $updateGenerator, WebhookManagerInterface $webhookManager)
    {
        $this->updateGenerator = $updateGenerator;
        $this->webhookManager = $webhookManager;
    }

    /**
     * @inheritdoc
     */
    public function getUpdates(): \Generator
    {
        yield $this->updateGenerator->getUpdates();
    }

    /**
     * @inheritdoc
     */
    public function setWebhook(string $url, string $certificate = null): string
    {
        return $this->webhookManager->setWebhook($url);
    }

    /**
     * @inheritdoc
     */
    public function handleWebhook(string $json): UpdateInterface
    {
        return $this->webhookManager->handleWebhook($json);
    }
}
