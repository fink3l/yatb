<?php

declare(strict_types=1);

namespace Yatb\Exception;

use Yatb\Api;
use Yatb\Client;
use Yatb\Model\Update;
use Yatb\Service\ApiModelFactory;
use Yatb\Service\ApiUpdateGenerator;
use Yatb\Service\FakeUpdateGenerator;
use Yatb\Service\WebhookManager;

class Exception extends \Exception
{

}
