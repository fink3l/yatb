<?php

namespace Yatb;

use Yatb\Service\UpdateGeneratorInterface;
use Yatb\Service\WebhookManagerInterface;

interface ClientInterface extends UpdateGeneratorInterface, WebhookManagerInterface
{
}
