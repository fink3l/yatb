<?php

namespace Yatb\Service;

use Yatb\Model\ApiInterface;

interface ApiModelFactoryInterface
{
    /**
     * @param string $className
     * @param string $json
     *
     * @return ApiInterface
     */
    public function create(string $className, string $json): ApiInterface;
}
