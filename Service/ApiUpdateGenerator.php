<?php

declare(strict_types=1);

namespace Yatb\Service;

use TelegramBot\Api\Exception;
use Yatb\ApiInterface;
use Yatb\Exception\GetUpdatesException;

class ApiUpdateGenerator implements UpdateGeneratorInterface
{
    /**
     * @var int
     */
    private $offset = 0;

    /**
     * @var int
     */
    private $limit = 1000;

    /**
     * @var ApiModelFactoryInterface
     */
    private $factory;

    /**
     * @var ApiInterface
     */
    private $api;

    /**
     * @var string
     */
    private $className;

    /**
     * @param string $className
     * @param ApiModelFactoryInterface $factory
     * @param ApiInterface $api
     *
     * @throws Exception
     */
    public function __construct(string $className, ApiModelFactoryInterface $factory, ApiInterface $api)
    {
        $this->className = $className;
        $this->factory = $factory;
        $this->api = $api;

        do {
            $updates = $this->api->getUpdates($this->offset, $this->limit, self::TIMEOUT);
            $this->offset = \count($updates);
        } while ($this->offset !== $this->limit);
    }

    /**
     * @inheritdoc
     */
    public function getUpdates(): \Generator
    {
        while (true) {
            try {
                $updates = $this->api->getUpdates($this->offset, $this->limit, self::TIMEOUT);
            } catch (Exception $e) {
                throw new GetUpdatesException($e->getMessage());
            }

            if (array_key_exists($this->offset, $updates)) {
                $update = $updates[$this->offset];
                ++$this->offset;

                yield $this->factory->create($this->className, $update->toJson());
            }
        }
    }
}
