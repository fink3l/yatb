<?php

declare(strict_types=1);

namespace Yatb\Service;

use Yatb\Model\ApiInterface;
use Yatb\Exception\CreateException;

class ApiModelFactory implements ApiModelFactoryInterface
{
    public function create(string $className, string $json): ApiInterface
    {
        /** @var ApiInterface $object */
        $object = new $className();

        if (null === ($data = json_decode($json))) {
            $this->throwCreateException(sprintf('invalid json: %s', $json));
        }

        return $object->setData($data);
    }

    /**
     * @param string $message
     *
     * @throws CreateException
     */
    private function throwCreateException(string $message)
    {
        throw new CreateException($message);
    }

}