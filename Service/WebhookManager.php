<?php

declare(strict_types=1);

namespace Yatb\Service;

use Yatb\ApiInterface;
use Yatb\Model\UpdateInterface;

class WebhookManager implements WebhookManagerInterface
{
    /**
     * @var string
     */
    private $className;

    /**
     * @var ApiModelFactoryInterface
     */
    private $factory;

    /**
     * @var ApiInterface
     */
    private $api;

    /**
     * @param string $className
     * @param ApiModelFactoryInterface $factory
     * @param ApiInterface $api
     */
    public function __construct(string $className, ApiModelFactoryInterface $factory, ApiInterface $api)
    {
        $this->className = $className;
        $this->factory = $factory;
        $this->api = $api;
    }

    /**
     * @inheritdoc
     */
    public function setWebhook(string $url, string $certificate = null): string
    {
        return $this->api->setWebhook($url, $certificate);
    }

    /**
     * @inheritdoc
     */
    public function handleWebhook(string $json): UpdateInterface
    {
        return $this->factory->create($this->className, $json);
    }
}
