<?php

namespace Yatb\Service;

use Yatb\Exception\CreateException;
use Yatb\Exception\GetUpdatesException;
use Yatb\Model\UpdateInterface;

interface UpdateGeneratorInterface
{
    public const TIMEOUT = 0;

    /**
     * @throws GetUpdatesException
     * @throws CreateException
     *
     * @return \Generator|UpdateInterface[]
     */
    public function getUpdates(): \Generator;
}
