<?php

declare(strict_types=1);

namespace Yatb\Service;

class FakeUpdateGenerator implements UpdateGeneratorInterface
{
    /**
     * @var ApiModelFactoryInterface
     */
    private $factory;

    /**
     * @var string
     */
    private $className;

    /**
     * @param string $className
     * @param ApiModelFactoryInterface $factory
     */
    public function __construct(string $className, ApiModelFactoryInterface $factory)
    {
        $this->factory = $factory;
        $this->className = $className;
    }

    /**
     * @inheritdoc
     */
    public function getUpdates(): \Generator
    {
        while (true) {
            sleep(self::TIMEOUT);
            yield $this->factory->create($this->className, json_encode(
                [
                    'text' => '/hello',
                    'type' => 'command',
                    'command' => 'hellp',
                    'userId' => 1,
                    'username' => 'Albert Einstein',
                    'chatId' => 1,
                ]
            ));
        }
    }
}
