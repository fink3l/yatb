<?php

namespace Yatb\Service;

use Yatb\Model\UpdateInterface;

interface WebhookManagerInterface
{
    /**
     * @param string $url
     * @param string|null $certificate
     *
     * @throws \TelegramBot\Api\Exception
     *
     * @return string
     */
    public function setWebhook(string $url, string $certificate = null): string;

    /**
     * @param string $json
     *
     * @return UpdateInterface
     */
    public function handleWebhook(string $json): UpdateInterface;
}
