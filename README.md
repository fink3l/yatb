# Yet Another Telegram Bot

Yet another telegram bot api implementation
### Usage
```
$factory = new ApiModelFactory();
$api = new Api('bot-token');
$generator = new ApiUpdateGenerator(Update::class, $factory, $api);

foreach ($generator->getUpdates() as $update) {
    // do something with update
}
```

### TestGenerator component
##### interchangeable components
```
$fakeGenerator = new FakeUpdateGenerator(Update::class, $factory);

foreach ($fakeGenerator->getUpdates() as $fakeUpdate) {
    // do something with fake updates
}
```
##### use components separately or build your awesome monstro good object telegram client

```
$webhook = new WebhookManager(Update::class, $factory, $api);

$client = new Client(random_int(0, 1) ? $generator : $fakeGenerator, $webhook);

$client->setWebhook('.../weebhook');
$update = $client->handleWebhook($request);
// do something with update

$client->stopWebhook();

foreach ($client->getUpdates() as $fakeUpdate) {
    // do something with fake updates
}
```